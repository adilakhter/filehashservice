import com.typesafe.sbt.SbtStartScript

seq(SbtStartScript.startScriptForClassesSettings: _*)

name := "FileHashService"

organization := "org.xiaon"

version := "1.0.0-SNAPSHOT"

scalaVersion := "2.11.2"

resolvers += "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq(
  "com.typesafe.akka"      %% "akka-actor"            % "2.3.6",
  "com.typesafe.akka"      %% "akka-slf4j"            % "2.3.6",
  "com.typesafe.akka"      %% "akka-testkit"          % "2.3.6"      % "test",
  "io.spray"               %% "spray-can"             % "1.3.1",
  "io.spray"               %% "spray-client"          % "1.3.1",
  "io.spray"               %% "spray-routing"         % "1.3.1",
  "io.spray"               %% "spray-json"            % "1.2.6",
  "io.spray"               %% "spray-testkit"         % "1.3.1"        % "test",
  "org.scalatest"          %% "scalatest"             % "2.2.1"        % "test",
  "org.specs2"             %% "specs2"                % "2.4.2"        % "test",
  "ch.qos.logback"         % "logback-classic"        % "1.1.2",
  "com.google.guava"       % "guava"                  % "18.0"
)

mainClass := Some("org.xiaon.hashservice.Main")
    