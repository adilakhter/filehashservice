logLevel := Level.Warn

/**
 * SbtPlugin to generate start.bat script to run this application
 * without loading SBT in production.
 */
addSbtPlugin("com.typesafe.sbt" % "sbt-start-script" % "0.10.0")