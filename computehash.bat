@echo off

IF [%1%] EQU [] (ECHO Please specify input file path. This script requires a  file path as the first argument. && EXIT 1)
IF [%2%] EQU [] (ECHO Please specify output file path.  This script requires a file path as the second argument. && EXIT 1)


SET MAIN_CLASS_QNAME=org.xiaon.hashservice.Main
SET IN_FILE_PATH=%1
SET OUT_FILE_PATH=%2

(ECHO Initiating FileHashService with input: %1 and output: %2...)

SHIFT
target/start.bat %MAIN_CLASS_QNAME% %IN_FILE_PATH% %OUT_FILE_PATH%