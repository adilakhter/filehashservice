# FileHashService 

## Introduction 

An actor based hash service application to compute hash of lines of text based input file. 
It utilizes a rest based service to compute the hash of strings. In essence, it reads a file and 
request the service to compute hash of a block (which we call record) of data. Upon receiving the response, it writes the hash of the record 
in the specified output file in a `ordered` manner. That is, if a file contain following line content in a sequence, `{"A" , "B" , "C"}`, the output 
in the same order, as in `{"hashOf-A", "hashOf-B", "hashOf-C"}`. 

### Main characteristics:

- It utilizes akka's work-pulling pattern [4]  with master and workers (see `ServiceMaster.scala` and `HashComputationWorker.scala`).
- It writes output while retaining the original order of the sequence of lines.(see `OutputProcess.scala`).
- To do above, it utilizes some coordination among the actors and intermediate data structure such as TreeMap, Queue . 
- In addition, it also applies the shutdown pattern to stop the actor system of `FileHashService` (see `Terminator.scala`) [5].   

### Main Actors:

Among others, following are the notable actors: 

- ServiceMaster: Master actor for the `FileHashService` application. It pulls the works from the `FileReader` actor based on the need and let `HashComputationWorker` computes the corresponding hash and finally utilizes the `OutputWriter` to write it in the output file in the *same* sequence as it was *read*.
- FileReader: A simple actor that reads the input file lazily and produces `DataRecords` to subsequently processed by this application. 
- OutputWriter: An actor that processes the compute `HashedRecord` and serializes it to the output fie.  
- HashComputationWorker: It computes hash of `DataRecords` and generates corresponding `HashedRecords` by utilizing the provided`hashing-as-a-service` spray endpoint.

### Configuration

Using akka's `application.conf` several parameter of the application can be configured. Notable configurable options are :

* *org.xiaon.hashservice*:
  - `infile.encoding` : Input file encoding
  - `outfile.encoding`: Output file encoding
  - `recordsize`: No of lines in each `Record` (ie #lines retrieved and processed for hash computation as a unit) from the source file at each instance of read.
  - `inital-no-of-records-to-begin`: No of records to be read when the computation begins.
  - `hashing-as-a-service.url` : Url to the rest service that compute hash.
  - `hashing-as-a-service.max-retries`:  No of attempts to communicate with the service by each worker.
  - `hashcomputationworkers.noOfWorkers`:  No of Workers that compute hash.


In addition, logging can also be configured by setting the values accordingly in `application.conf ` and `logback.xml` file.


## Instructions 

### Prerequisites:

As the project is sbt-based, please make sure a recent version of SBT is installed. For instructions on how to install and quickly configure SBT, please visit: [1]. Please note that the provided url may have changed with time.


### Compilation  

In order to compile this application, please follow the steps below. 

* Clone this git repository and checkout [release/0.1 branch](https://bitbucket.org/adilakhter/filehashservice/src/ec38a0fbf272e3e494d9184c2d88f02b4f2d75ff/?at=release%2F0.1). 
* Change directory to the project root with the command line.
* Invoke the following `sbt` command.
 
```
sbt start-script
```

It will package the application and generate the necessary scripts to run this application.
 

### Running this Application 

1. First, please ensure that the hashing-as-a-service is running and the following URL is accessible:[http://localhost:9000/api/service](http://localhost:9000/api/service). 
If the service is running with a different URL, please specify it in `application.conf` file. For more details, please visit [3].  

2. Please use the following command to run this application.  
 
```
computehash.bat .\input .\output
```

Note that we are using `sbt-start-script` plugin, which allows to run this application without loading SBT in the production. It is due to the fact that `sbt run` is not recommended for production use because it keeps SBT itself in-memory [2]. 


## Improvement to consider

We consider the following improvements in the future work. 

* Adaptive configuration based on the file size and processing power of the current workstation.
* Use remote actors in the context of scaling out scenerios. 
* Better exception handling. 
* More tests to cover the interaction among the `Master` and `HashComputationWorkers`.

## References

1. [www.scala-sbt.org](http://www.scala-sbt.org/)
2. [github.com/sbt/sbt-start-script](https://github.com/sbt/sbt-start-script) 
3. [github.com/ajantis/simple-sample-service](https://github.com/ajantis/simple-sample-service)
4. [Work-Pulling Pattern](http://letitcrash.com/post/29044669086/balancing-workload-across-nodes-with-akka-2)     
5. [Shutdown Patterns](http://letitcrash.com/post/30165507578/shutdown-patterns-in-akka-2) 
