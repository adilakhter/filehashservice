package org.xiaon.hashservice.actors

import akka.actor.Props
import akka.testkit.{TestActorRef, TestProbe}
import org.scalatest.BeforeAndAfterEach
import org.xiaon.hashservice.actors.FileReader.InitializeReader
import org.xiaon.hashservice.actors.OutputProcessor.InitializeWriter
import org.xiaon.hashservice.actors.ServiceMaster.BeginHashComputation
import org.xiaon.hashservice.providers.{HashProcessorProvider, ReaderProvider}
import org.xiaon.hashservice.utils.TestKitSpec

import scala.concurrent.duration._
/**
 * The ServiceMasterSpec
 *
 * @author Adil Akhter
 */
class ServiceMasterSpec extends TestKitSpec("HashServiceMasterSpec") with BeforeAndAfterEach {

  trait TestCase {
    val readerProbe  = TestProbe()
    val processorProbe = TestProbe()

    trait TestReaderProvider extends ReaderProvider{
      override def reader = readerProbe.ref
      override def readerPath = readerProbe.ref.path
    }
    trait TestHashProcessorProvider extends HashProcessorProvider{
      override def processor = processorProbe.ref
      override def processorPath = processorProbe.ref.path
    }

    val ref = TestActorRef(
                Props(
                  new ServiceMaster
                    with TestReaderProvider
                    with TestHashProcessorProvider))
  }

  "A HashServiceMaster" should{
    "get configured the encodings after initialization" in new TestCase{
      val master = ref.underlyingActor.asInstanceOf[ServiceMaster]

      master.inFileEncoding should_!= ""
    }
  }

  "A HashServiceMaster" should{
    "send initialize reader message after being configured" in new TestCase{
      val master = ref.underlyingActor.asInstanceOf[ServiceMaster]

      val inputFilePath = "./temp.log"
      val outputFilePath = "./out.log"

      ref ! BeginHashComputation(inputFilePath,outputFilePath)

      readerProbe.fishForMessage(10 seconds){
        case InitializeReader(inputFilePath, master.inFileEncoding) => true
        case _ => false
      }
    }
    "send initialize writer to the hash processor after being configured" in new TestCase{
      val master = ref.underlyingActor.asInstanceOf[ServiceMaster]

      val inputFilePath = "./temp.log"
      val outputFilePath = "./out.log"

      ref ! BeginHashComputation(inputFilePath,outputFilePath)

      processorProbe.fishForMessage(10 seconds){
        case InitializeWriter(outputFilePath, master.outFileEncoding) => true
        case _ => false
      }
    }
    "initialization changes the status to initialized" in new TestCase{
      val master = ref.underlyingActor.asInstanceOf[ServiceMaster]

      val inputFilePath = "./temp.log"
      val outputFilePath = "./out.log"

      ref ! BeginHashComputation(inputFilePath,outputFilePath)

      processorProbe.expectMsg(InitializeWriter(outputFilePath, master.outFileEncoding))
      readerProbe.expectMsg(InitializeReader(inputFilePath, master.inFileEncoding))
    }
  }
}
