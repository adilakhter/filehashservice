package org.xiaon.hashservice.actors

import akka.actor.{ActorPath, Props}
import akka.testkit.{TestActorRef, TestProbe}
import org.xiaon.hashservice.actors.FileReader._
import org.xiaon.hashservice.model.DataRecord
import org.xiaon.hashservice.providers.ServiceMasterProvider
import org.xiaon.hashservice.utils.TestKitSpec


import scala.concurrent.duration._

/**
 * The FileReaderSpec tests FileReader actor.
 *
 * @author Adil Akhter
 */
class FileReaderSpec extends TestKitSpec("FileReaderWorkerSpec"){
  val EMPTY_DATA_FILE = getClass.getResource("/data/empty.log")
  val TEST_DATA_FILE = getClass.getResource("/data/test.log")

  trait TestCase{
    val probe = TestProbe()
    trait TestMasterProvider extends ServiceMasterProvider {
      override def master = probe.ref
      override def masterPath: ActorPath = probe.ref.path
    }
    val ref = TestActorRef(Props(new FileReader with TestMasterProvider))
  }

  "A FileReaderWorker initilization" should {
    "get into active state if uninitialized" in new TestCase{
      val expectedMessage = ReaderIsInitializedWith(TEST_DATA_FILE.getPath, "UTF-8")
      ref ! InitializeReader(TEST_DATA_FILE.getPath, "UTF-8")
      probe.expectMsg(expectedMessage)
    }
    "setup iterator for the input data file" in new TestCase{
      val expectedMessage = ReaderIsInitializedWith(TEST_DATA_FILE.getPath, "UTF-8")
      ref ! InitializeReader(TEST_DATA_FILE.getPath, "UTF-8")

      probe.expectMsg(expectedMessage)

      val reader = ref.underlyingActor.asInstanceOf[FileReader]
      reader.linesToProcess.toList.length should_==3
    }
    "get initialized `once` if uninitialized" in new TestCase{
      ref ! InitializeReader(TEST_DATA_FILE.getPath, "UTF-8")

      val expectedMessage1 = ReaderIsInitializedWith(TEST_DATA_FILE.getPath, "UTF-8")
      probe.expectMsg(expectedMessage1)

      ref ! InitializeReader("test2.log", "UTF-8")
      val expectedMessage2 = ReaderAlreadyInitializedWith(TEST_DATA_FILE.getPath, "UTF-8")
      probe.expectMsg(expectedMessage2)
    }
    "get initialized `twice` does not change internal state" in new TestCase{
      val filePath = TEST_DATA_FILE.getPath
      val encoding = "UTF-8"

      ref ! InitializeReader(filePath,encoding)
      ref ! InitializeReader("test2.log",encoding)

      val reader = ref.underlyingActor.asInstanceOf[FileReader]
      reader.filePath should_==filePath
      reader.fileEncoding should_==filePath
    }
  }
  "A FileReaderWorker read" should {
    "get data in chunk" in  new TestCase{
      val encoding = "UTF-8"

      ref ! InitializeReader(TEST_DATA_FILE.getPath,encoding)
      ref ! ReadData(1)

      probe.fishForMessage(3 seconds){
        case ReceiveDataRecords(List(DataRecord(1, List("1")))) => true
        case _ => false
      }
    }
    "subsequent return next data in chunk" in  new TestCase{
      val encoding = "UTF-8"

      ref ! InitializeReader(TEST_DATA_FILE.getPath,encoding)
      ref ! ReadData(1)
      ref ! ReadData(1)
      ref ! ReadData(10)

      probe.fishForMessage(10 seconds){
        case ReceiveDataRecords(List(DataRecord(3, List("3")))) => true
        case _ => false
      }
    }
    "EOF should send ReadCompletedFeedback" in  new TestCase{
      val encoding = "UTF-8"

      ref ! InitializeReader(TEST_DATA_FILE.getPath,encoding)
      ref ! ReadData(3)
      ref ! ReadData(3)

      probe.fishForMessage(10 seconds){
        case FileReadCompleted(_) => true
        case _ => false
      }
    }
    "asking more returns only the lines available" in  new TestCase{
      val encoding = "UTF-8"

      ref ! InitializeReader(TEST_DATA_FILE.getPath,encoding)
      ref ! ReadData(1)
      ref ! ReadData(1)
      ref ! ReadData(10)

      probe.fishForMessage(10 seconds){
        case ReceiveDataRecords(List(DataRecord(3, List("3")))) => true
        case _ => false
      }

      ref ! ReadData(10)

      probe.expectMsg(FileReadCompleted(3))
    }
  }
}



