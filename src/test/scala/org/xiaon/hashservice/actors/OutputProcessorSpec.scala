package org.xiaon.hashservice.actors

import akka.actor.{ActorPath, ActorRef, Props}
import akka.pattern.gracefulStop
import akka.testkit._
import org.scalatest.BeforeAndAfterEach
import org.xiaon.hashservice.actors.OutputProcessor._
import org.xiaon.hashservice.actors.ServiceMaster.{RequestWork, WorkIsAvailable}
import org.xiaon.hashservice.io._
import org.xiaon.hashservice.model.HashedRecord
import org.xiaon.hashservice.providers._
import org.xiaon.hashservice.utils.TemporaryFolder
import org.xiaon.hashservice.utils.TestKitSpec

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

/**
 * A OutputProcessorSpec that tests OutputProcessor
 * and whether it is writing output in the order
 * derived from the input file.
 *
 * @author Adil Akhter
 */
class OutputProcessorSpec
  extends TestKitSpec("OutputProcessorSpec")
  with TemporaryFolder
  with BeforeAndAfterEach {

  var ref: ActorRef = _
  trait TestCase {
    val OUTPUT_FILE_PATH:String = newFilePath
    val ANOTHER_OUTPUT_FILE_PATH:String = newFilePath

    val probe = TestProbe()
    trait TestMasterProvider extends ServiceMasterProvider {
      override def master = probe.ref
      override def masterPath: ActorPath = probe.ref.path
    }

    ref = TestActorRef(Props(new OutputProcessor with TestMasterProvider with FileIOHelper))
  }

  override def afterEach() {
    try {
      val stopped: Future[Boolean] = gracefulStop(ref, 3 seconds, system)
      Await.result(stopped, 4 seconds)
    } catch {
      case e: akka.pattern.AskTimeoutException =>
    }
  }

  "A OrderedHashProcessor initialization" should {
    "get into active state if uninitialized" in new TestCase {
      val expectedMessage = WriterIsInitializedWith(OUTPUT_FILE_PATH, "UTF-8")
      ref ! InitializeWriter(OUTPUT_FILE_PATH, "UTF-8")
      probe.expectMsg(expectedMessage)
    }
    "send `already initialized` in case of multiple InitializeWriter message" in new TestCase {
      val initResponse = WriterIsInitializedWith(OUTPUT_FILE_PATH, "UTF-8")
      val alreadyInitedResponse = WriterAlreadyInitializedWith(OUTPUT_FILE_PATH, "UTF-8")

      ref ! InitializeWriter(OUTPUT_FILE_PATH, "UTF-8")
      probe.expectMsg(initResponse)

      // HashProcessor has already been initialized.
      // We are sending a second InitializeWriter message, which
      // should results in
      ref ! InitializeWriter(ANOTHER_OUTPUT_FILE_PATH, "UTF-16")
      probe.expectMsg(alreadyInitedResponse)
    }
  }
  "A OrderedHashProcessor write" should {
    "serialize data in order" in new TestCase {
      val initResponse = WriterIsInitializedWith(OUTPUT_FILE_PATH, "UTF-8")

      ref ! InitializeWriter(OUTPUT_FILE_PATH, "UTF-8")
      probe.expectMsg(initResponse)


      val processDataWithID1 = ProcessHashedData(HashedRecord(1, List("Line1" , "Line2" , "Line3")))
      val processDataWithID2 = ProcessHashedData(HashedRecord(2, List("Line4" , "Line5" , "Line6")))
      val processDataWithID3 = ProcessHashedData(HashedRecord(3, List("Line7" , "Line8" , "Line9")))

      ref ! WorkIsAvailable
      probe.expectMsg(RequestWork(ref))
      ref ! processDataWithID2

      probe.expectMsg(ProcessingHashedDataCompleted(None))
      probe.expectMsg(RequestWork(ref))

      ref ! processDataWithID3

      probe.expectMsg(ProcessingHashedDataCompleted(None))
      probe.expectMsg(RequestWork(ref))

      ref ! processDataWithID1

      val expectedHashedDataIDs = List(1,2,3)
      var actualHashedDataIDs = List.empty[Long]

      probe.fishForMessage(10 seconds){
        case ProcessingHashedDataCompleted(Some(ids)) =>
          actualHashedDataIDs = ids
          true
        case _ => false
      }
      actualHashedDataIDs should contain(containTheSameElementsAs(expectedHashedDataIDs))
    }
  }
}