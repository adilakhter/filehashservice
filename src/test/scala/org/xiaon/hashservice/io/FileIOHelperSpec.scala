package org.xiaon.hashservice.io

import org.specs2.mutable.Specification

class FileIOHelperSpec extends Specification {
  val ioHelper = new FileIOHelper{}

  "FileIOHelper.verifyFileExists" should {
    "return true in case of file exists" in {

      val url = getClass.getResource("/data/empty.log");
      val expected = true
      val actual = ioHelper.verifyFileExists(Some(url.getPath))

      actual should_== expected
    }

    "return false in case of non-existent file" in {

        val expected = false
        val actual = ioHelper.verifyFileExists(Some("unknown_file"))

        actual should_== expected
    }
  }
}
