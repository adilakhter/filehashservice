package org.xiaon.hashservice.utils

import akka.actor.{Actor, ActorRef}

/**
 *
 * @author Adil Akhter
 */
class TestProbeWrapper(target: ActorRef) extends Actor {
  def receive = {
    case x => target forward x
  }
}
