package org.xiaon.hashservice.utils

import java.io.File
import java.util.UUID

import org.xiaon.hashservice.io.FileIOHelper

trait TemporaryFolder{
  import TemporaryFolder._

  lazy val tempDir = {
    val dir = File.createTempFile("test", "")
    dir.delete
    dir.mkdir
    dir
  }

  def newFilePath: String = {
    val filePath: String = System.getProperty("user.dir") + "/target/test_output_temp"+index+".log"
    index = index + 1
    new FileIOHelper{}.ensureDirectoriesExists(filePath)
    filePath

//    val f = new File(tempDir.getPath+"/"+UUID.randomUUID.toString+".log")
//    f.createNewFile
//    f
  }
  def delete = {
    Option(tempDir.listFiles).map(_.toList).getOrElse(Nil).foreach(_.delete)
    tempDir.delete
  }
}

object TemporaryFolder{
  var index = 0
}