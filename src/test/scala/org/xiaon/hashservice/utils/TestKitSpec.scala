package org.xiaon.hashservice.utils

import akka.actor.ActorSystem
import akka.event.Logging
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.{MustMatchers, WordSpecLike}
import org.specs2.matcher.ShouldMatchers

import scala.collection.mutable
import scala.concurrent.duration.Duration
import scala.concurrent.duration._

/**
 *
 * @author Adil Akhter
 */
abstract class TestKitSpec(name: String)
  extends TestKit(ActorSystem(name))
  with WordSpecLike
  with ShouldMatchers
  with ImplicitSender
  with ShutdownSystemAfterAll {

}
