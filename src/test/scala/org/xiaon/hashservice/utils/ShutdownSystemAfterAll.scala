package org.xiaon.hashservice.utils
import org.scalatest.{Suite, BeforeAndAfterAll}
import akka.testkit.TestKit

/**
 *
 * @author Adil Akhter
 */
trait ShutdownSystemAfterAll extends BeforeAndAfterAll{
  this: TestKit with Suite =>
  override protected def afterAll(): Unit = {
    super.afterAll()
    system.shutdown()
  }
}

