package org.xiaon.hashservice.collection

import org.scalatest.BeforeAndAfter
import org.specs2.mutable.Specification
import org.xiaon.hashservice.io.FileIOHelper
import org.xiaon.hashservice.model.HashedRecord

/**
 * The OrderedHashDataMapSpec
 *
 * @author Adil Akhter
 */
class OrderedHashDataMapSpec  extends Specification{
  "OrderedHashedDataMap.enqueue" should {
    "hashed data by its id" in{
      val queue = new OrderedHashedDataQueue
      val dataToAdd = HashedRecord(1, List("hash1"))

      queue.enqueue(dataToAdd)

      queue.treeMap.firstKey should_== dataToAdd.id
    }
    "sort data by id" in {
      val queue = new OrderedHashedDataQueue

      val dataToAdd1 = HashedRecord(10, List("hash1"))
      val dataToAdd2 = HashedRecord(5, List("hash2"))
      val dataToAdd3 = HashedRecord(1, List("hash3"))

      queue.enqueue(dataToAdd1)
      queue.enqueue(dataToAdd2)
      queue.enqueue(dataToAdd3)

      queue.treeMap.firstKey should_== dataToAdd3.id
    }
  }
  "OrderedHashedDataMap.enqueueAll" should{
    "behaves similar to enqueue" in{
      val queue = new OrderedHashedDataQueue

      val dataToAdd1 = HashedRecord(10, List("hash1"))
      val dataToAdd2 = HashedRecord(5, List("hash2"))
      val dataToAdd3 = HashedRecord(1, List("hash3"))

      queue.enqueueAll(List(dataToAdd1, dataToAdd2, dataToAdd3))
      queue.treeMap.firstKey should_== dataToAdd3.id
    }
  }
  "OrderedHashedDataMap.dequeue" should {
    "return data based on the predicate specified" in {
      val queue = new OrderedHashedDataQueue
      val dataToAdd1 = HashedRecord(3, List("hash1"))
      val dataToAdd2 = HashedRecord(2,  List("hash2"))
      val dataToAdd3 = HashedRecord(1,  List("hash3"))

      queue.enqueueAll(List(dataToAdd1, dataToAdd2, dataToAdd3))

      val actual: List[HashedRecord] = queue.dequeue{case head@(id, data) => id == 1}
      actual.head should_== dataToAdd3
    }
    "return empty list if head is not `nextHashDataToProcess`" in {
      val queue = new OrderedHashedDataQueue
      val nextHashDataToProcess = 1

      val dataToAdd1 = HashedRecord(3, List("hash1"))
      val dataToAdd2 = HashedRecord(2,  List("hash2"))

      queue.enqueueAll(List(dataToAdd1, dataToAdd2))

      val actual: List[HashedRecord] = queue.dequeue{case head@(id, data) => id == nextHashDataToProcess}
      actual should_== List.empty
    }
    "return all when `nextHashDataToProcess` become avialable" in {
      val queue = new OrderedHashedDataQueue
      var nextHashDataToProcess = 1
      def predicate: Tuple2[Long,HashedRecord] => Boolean = {
        case head@(id, data) => {
          val isEqual= id == nextHashDataToProcess
          if(isEqual)
            nextHashDataToProcess = nextHashDataToProcess + 1 // proceed to process the next `HashedData`

          isEqual
        }
      }

      val dataToAdd1 = HashedRecord(3, List("hash1"))
      val dataToAdd2 = HashedRecord(2,  List("hash2"))
      val dataToAdd3 = HashedRecord(1,  List("hash1"))

      queue.enqueueAll(List(dataToAdd1, dataToAdd2))

      val intermediateResult: List[HashedRecord] = queue.dequeue(predicate)
      intermediateResult should_== List.empty

      queue.enqueue(dataToAdd3)

      val actual: List[HashedRecord] = queue.dequeue(predicate)

      println(actual)

      actual must contain(dataToAdd3, dataToAdd2, dataToAdd1).inOrder.exactly
      queue.treeMap.isEmpty should_== true
    }
  }
}
