package org.xiaon.hashservice.config

import org.specs2.mutable.Specification

/**
 * @author Adil Akhter
 */
class ConfigHelperSpec extends Specification {

  "ConfigHelper.getConfigOrElse" should {
    "return defaultValue in case of any Exception" in {
      val expected = "test"
      val actual = ConfigHelper.getConfigOrElse({throw new Exception("Exception in reading configuration")})(expected)
      actual must  beEqualTo(expected)
    }
    "return the configurationValue in case of no Exception" in {
      val expected = 10
      val actual = ConfigHelper.getConfigOrElse({10})(-1)

      actual must beEqualTo(expected)
    }
  }
}

