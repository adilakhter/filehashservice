package org.xiaon.hashservice

import akka.actor._
import akka.event.Logging
import akka.util.Timeout
import org.slf4j.LoggerFactory
import org.xiaon.hashservice.actors.ServiceMaster._
import org.xiaon.hashservice.actors._
import akka.pattern._
import scala.concurrent.duration._
import scala.concurrent.Await

/**
 * The MainApp that runs the FileHashService
 * with the specified `inputFile` and `outputFile`
 * specified in the code below.
 *
 * Note that it is only for the testing purpose. In
 * order to run this service, please either use
 * ./computehash.bat or Main.scala.
 *
 * @author Adil Akhter
 */
object MainApp extends  App {
  val inputFile = "./log/application_test.log"
  val outputFile = "./log/output.txt"

  val system = ActorSystem("FileHashServiceSystem")
  val log = Logging(system, MainApp.getClass)

  // Configuring default timeout
  implicit val timeout =  Timeout(5.seconds)
  val serviceMaster = system.actorOf(Props(ServiceMaster()), BaseActor.SERVICE_MASTER)

  system.actorOf(Props(Terminator(serviceMaster)), BaseActor.TERMINATOR_ID)

  Await.result(serviceMaster? BeginHashComputation(inputFile, outputFile), 3 seconds)

  println(s"Processing started ${inputFile}")
}
