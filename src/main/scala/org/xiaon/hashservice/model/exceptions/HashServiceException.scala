package org.xiaon.hashservice.model.exceptions

/**
 * An exception that acts as a base exception and forms an exception hierarchy.
 *
 * @author Adil Akhter
 */
class HashServiceException(message: String = null, cause: Throwable = null) extends  Exception(message, cause)

class IllegalMessageException(message: String = null, cause: Throwable = null) extends  HashServiceException(message, cause)

class ServiceRequestFailedException(message: String = null, cause: Throwable = null) extends  HashServiceException(message, cause)



