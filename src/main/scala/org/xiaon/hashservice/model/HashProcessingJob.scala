package org.xiaon.hashservice.model

/**
 * A class that represents the processing request send  to the
 * the webservice. It in essence implies and unit of
 * hash computation work for the webservice.
 *
 * @param id id of the job
 * @param lines a seq of strings where each string represents the
 *              line in the input file.
 * @author Adil Akhter
 */
case class HashProcessingJob(id: String, lines: List[String])

/**
 * A class that constructs JSON response received from the
 * REST-based webservice.
 *
 * @param id job id
 * @param lines a seq of hashed strings. Order is same as specified
 *              in the `HashProcessingJob`.
 */
case class HashProcessingJobResponse(id: String, lines: List[String])

/**
 * A case class that builds a HashProcessingJob to retry in case of
 * any service failure.
 *
 * @param retryCount no of time the request is attempted in case of exception.
 * @param job an instance of `HashProcessingJob`.
 */
case class RetryHashProcessingJob(retryCount: Int, job: HashProcessingJob)




