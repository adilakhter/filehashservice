package org.xiaon.hashservice.model

/**
 * A case class that constructs the unit of work to compute
 * hash.
 *
 * @param recId id of an instance of DataRecord.
 * @param lines a sequence of strings representing lines to be hashed.
 */
case class DataRecord(recId: Long, lines: List[String])
