package org.xiaon.hashservice.model

/**
  *  A case class that acts as a container for the resultant hash
  *  of a record read from the input source.
  *
  *  It contains the original record `id` and `result`
  *
  * @author Adil Akhter
  */
case class HashedRecord(id: Long, hash: List[String])