package org.xiaon.hashservice.io

/**
 * A helper class that utilizes loan pattern
 * to close resources after processing is completed.
 *
 * @author Adil Akhter
 */
object ResourceManager {
  def using[A <: {def close() : Unit}, B](resource: A)(f: A => B): B =
    try {
      f(resource)
    } finally {
      resource.close()
    }
}
