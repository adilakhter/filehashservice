package org.xiaon.hashservice.io

import scala.io.Source
import org.slf4j.LoggerFactory


/**
 *  The IOHelper
 *
 * @author Adil Akhter
 */
trait IOHelper {
  def verifyFileExists(path: Option[String]): Boolean
  def ensureDirectoriesExists(path: String): Unit
}

