package org.xiaon.hashservice.io

import org.slf4j.LoggerFactory

/**
 * The FileIOHelper
 *
 * @author Adil Akhter
 */
object FileIOHelper{
  val LOG = LoggerFactory.getLogger(FileIOHelper.getClass)
}

trait FileIOHelper extends IOHelper{
  import FileIOHelper.LOG

  override def verifyFileExists(path: Option[String]): Boolean = {
    val inputFile = path.map(name => new java.io.File(name))
    inputFile match{
      case Some(file) if file.exists() => true
      case _                           => false
    }
  }

  override def ensureDirectoriesExists(path: String): Unit = {
    val outputFile: java.io.File = new java.io.File(path)
    if(!outputFile.exists()){
      LOG.info("{} does not exists ", outputFile.getAbsolutePath)
      LOG.info("Creating necessary directory structure ", outputFile.getAbsolutePath)
      com.google.common.io.Files.createParentDirs(outputFile)
    }
  }

}
