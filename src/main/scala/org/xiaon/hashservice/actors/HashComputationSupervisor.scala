package org.xiaon.hashservice.actors

import akka.actor.Props
import org.xiaon.hashservice.actors.utils.{OneForOneStrategyFactory, SupervisionStrategyFactory, WorkerCreationPolicy}
import org.xiaon.hashservice.providers.DefaultMasterProvider

/**
 * Supervisor that keeps monitor the hash computation workers. It
 * employs restart supervisor behavior which restarts that
 * child actors in case of any exception.
 *
 * It is particularly useful in case of `HashComputationWorker` as
 * due to any sort of exception it simply can spawn a new worker.
 *
 * @author Adil Akhter
 */
class HashComputationSupervisor extends RestartSupervisor {
  this: DefaultMasterProvider with SupervisionStrategyFactory with WorkerCreationPolicy =>

  /**
   * Defines the child actors of this supervisor.
   *
   * In particular, it defines all the HashComputationWorkers
   * based on how it is prescribed in the configuration file.
   */
  override protected[this] def startWorkers(): Unit = {
    log.debug("Configuring {} with '{}' number of workers with name starts with '{}'", self.path, noOfWorkers, workerNamePrefix)

    1 to noOfWorkers foreach{i => context.actorOf(Props(createWorker),s"$workerNamePrefix-$i")}
  }
}

object HashComputationSupervisor {
  def apply() = new HashComputationSupervisor with DefaultMasterProvider with WorkerCreationPolicy with  OneForOneStrategyFactory
}

