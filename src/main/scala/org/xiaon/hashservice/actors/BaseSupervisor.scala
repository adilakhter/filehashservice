package org.xiaon.hashservice.actors

import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor.{Actor, ActorInitializationException, ActorKilledException, ActorLogging}
import org.xiaon.hashservice.actors.utils.SupervisionStrategyFactory
import org.xiaon.hashservice.model.exceptions.IllegalMessageException

import scala.concurrent.duration._

trait BaseSupervisor extends BaseActor{
  
  protected[this] def startWorkers(): Unit

  override def receive: Receive = {
    case m =>
      log.error("Not expecting direct message {} to supervisor actor {}", m, self.path)
      throw new IllegalMessageException(message=s"Not expecting direct message $m to supervisor actor ${self.path}")
  }


  final override def preStart(){
    log.debug("Starting workers for {}.", self.path)
    startWorkers() 
  }

  final override def postRestart(reason: Throwable){} //override default preStart() call
  final override def preRestart(reason: Throwable, message: Option[Any]){} //override default child restart

}

/**
 *  A ResumeSupervisor that resumes in case of any exception
 */
abstract class ResumeSupervisor(maxNrRetries: Int = -1, withinTimeRange: Duration= Duration.Inf) extends BaseSupervisor{ this: SupervisionStrategyFactory =>
  override val supervisorStrategy = makeStrategy(maxNrRetries, withinTimeRange){
    case _: ActorInitializationException => Stop
    case _: ActorKilledException => Stop
    case _: Exception => Resume
    case _ => Escalate
  }
}

/**
 * Stops the childs actor in case of any exception.
 *
 */
abstract class StopSupervisor(maxNrRetries: Int = -1, withinTimeRange: Duration= Duration.Inf) extends BaseSupervisor{ this: SupervisionStrategyFactory =>
  override val supervisorStrategy = makeStrategy(maxNrRetries, withinTimeRange){
    case _: ActorInitializationException => Stop
    case _: ActorKilledException => Stop
    case _: Exception => Stop
    case _: Error => Stop
    case _ => Escalate
  }
}

/**
 * Restarts the child actors in case of any exception.
 *
 * @param maxNrRetries no of times to try
 * @param withinTimeRange within the time range
 */
abstract class RestartSupervisor(maxNrRetries: Int = 2, withinTimeRange: Duration= 5 seconds) extends BaseSupervisor{ this: SupervisionStrategyFactory =>
  override val supervisorStrategy = makeStrategy(maxNrRetries, withinTimeRange){
    case _: ActorInitializationException => Stop
    case _: ActorKilledException => Stop
    case ex: Exception =>
      log.error("Exception {}", ex.getMessage)
      Restart
    case err: Error =>
      log.error("Error {}", err.getMessage)
      Stop
    case _ => Escalate
  }
}