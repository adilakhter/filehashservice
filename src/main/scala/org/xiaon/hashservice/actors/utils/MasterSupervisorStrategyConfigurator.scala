package org.xiaon.hashservice.actors.utils

import akka.actor.SupervisorStrategy.Stop
import akka.actor.{AllForOneStrategy, SupervisorStrategy, SupervisorStrategyConfigurator}
import org.slf4j.LoggerFactory
import MasterSupervisorStrategyConfigurator._

/**
 * Supervisor strategy configurator for the guardian actor.
 *
 * @author Adil Akhter
 */
class MasterSupervisorStrategyConfigurator extends SupervisorStrategyConfigurator{
  override def create(): SupervisorStrategy = {
    AllForOneStrategy(){
      case err =>
        LOG.error("Unhandled error occurred. So shutting down all the Guardian Actors.", err)
        Stop
    }
  }
}

object MasterSupervisorStrategyConfigurator{
  val LOG = LoggerFactory.getLogger(MasterSupervisorStrategyConfigurator.getClass)
}
