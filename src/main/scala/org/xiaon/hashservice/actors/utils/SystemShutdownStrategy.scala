package org.xiaon.hashservice.actors.utils

import org.xiaon.hashservice.actors.BaseActor
import scala.concurrent.duration._

/**
 * Strategy that dictate how to turn of the
 * actor system when the processing is done
 *
 * @author Adil Akhter
 */
trait SystemShutdownStrategy { this: BaseActor =>
  protected[this] def shutdown(): Unit = context.system.shutdown()
  def shutdownTimeout: Duration = 5 seconds
}

trait DefaultShutdownStrategy extends  SystemShutdownStrategy { this: BaseActor =>
  override protected[this] def shutdown(): Unit = {
    context.system.shutdown()
    log.info("Shutting down the actor system...")
    println("Waiting to shutdown actor system...")
    context.system.awaitTermination(shutdownTimeout)
  }
}
