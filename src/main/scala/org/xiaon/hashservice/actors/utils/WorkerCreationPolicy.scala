package org.xiaon.hashservice.actors.utils

import org.xiaon.hashservice.actors.{HashComputationWorker, BaseActor}
import org.xiaon.hashservice.providers.DefaultMasterProvider

/**
 *
 * @author Adil Akhter
 */
trait WorkerCreationPolicy{ this: BaseActor =>
  val WORKER_CONFIG_KEY = s"$APP_CONFIG_KEY.hashcomputationworkers"

  val noOfWorkers  = config.getInt(s"$WORKER_CONFIG_KEY.noOfWorkers")
  val workerNamePrefix = config.getString(s"$WORKER_CONFIG_KEY.workerNamePrefix")

  def createWorker: BaseActor = HashComputationWorker()

}