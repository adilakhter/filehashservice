package org.xiaon.hashservice.actors

import org.xiaon.hashservice.actors.utils.{SupervisionStrategyFactory, WorkerCreationPolicy}
import org.xiaon.hashservice.model.exceptions.IllegalMessageException
import org.xiaon.hashservice.providers.DefaultMasterProvider

/**
 * An IOSupervisor is responsible for the actors
 * responsible for reading input and writing output.
 *
 * @author Adil Akhter
 */
class IOSupervisor extends StopSupervisor{this: DefaultMasterProvider  with SupervisionStrategyFactory with WorkerCreationPolicy  =>
  // TODO: Iteration 2: Integrate IOSupervisor with `ServiceMaster`

  override protected[this] def startWorkers(): Unit = {
    //TODO: Iteration 2: Use IOSupervisor to spawn `FileReader` and `OutputWriter`.
  }

  override  def receive: Receive = {
    case m =>
      log.error("Not expecting direct message {} to supervisor actor {}", m, self.path)
      throw new IllegalMessageException(message=s"Not expecting direct message $m to supervisor actor ${self.path}")
  }
}
