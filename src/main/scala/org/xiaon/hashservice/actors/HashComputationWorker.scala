package org.xiaon.hashservice.actors

import java.util.concurrent.Executors

import akka.actor.ActorRef
import org.xiaon.hashservice.actors.BaseActor.IdentifiableMessage
import org.xiaon.hashservice.actors.HashComputationWorker._
import org.xiaon.hashservice.actors.ServiceMaster.{RequestWork, WorkIsAvailable}
import org.xiaon.hashservice.config.ConfigHelper
import org.xiaon.hashservice.model._
import org.xiaon.hashservice.providers.{DefaultMasterProvider, ServiceMasterProvider}
import spray.client.pipelining._
import spray.http.HttpRequest
import spray.json.DefaultJsonProtocol

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}
import ConfigHelper._

/**
 * A Worker communicates with the `hashing-as-a-service` and compute the hash
 * of given block of lines.
 *
 * @author Adil Akhter
 */
class HashComputationWorker extends BaseActor { this: ServiceMasterProvider =>
  val execService = Executors.newCachedThreadPool()
  implicit val execContext = ExecutionContext.fromExecutorService(execService)

  protected final val SERVICE_CONFIG_KEY = s"$APP_CONFIG_KEY.hashing-as-a-service"

  // configuration values required for the functionality of the workers
  val serviceURL: String  = getConfigOrElse(config.getString(s"$SERVICE_CONFIG_KEY.url"))("http://localhost:9000/api/service")
  val maxNoOfRetry: Int  =  getConfigOrElse(config.getInt(s"$SERVICE_CONFIG_KEY.max-retries"))(5)

  /**
   * Json protocol for the request and response objects.
   */
  object HashProcessingJobProtocol extends DefaultJsonProtocol{
    implicit val jobFormat = jsonFormat2(HashProcessingJob.apply)
    implicit val jobResponseFormat = jsonFormat2(HashProcessingJobResponse.apply)
  }

  import HashProcessingJobProtocol._
  import spray.httpx.SprayJsonSupport._
  val pipeline: HttpRequest => Future[HashProcessingJobResponse] = sendReceive ~> unmarshal[HashProcessingJobResponse] // Spray pipeline to perform the post requests.

  def doWork(workSender: ActorRef, work: HashProcessingJob, noOfRetry: Int = maxNoOfRetry): Unit = {
    val responseFuture = pipeline {
      Post(serviceURL, work) // performs the service post requests
    }
    responseFuture onComplete {
      case Success(x) =>
        log.info("Got response for record with ID: {}", x.id)
        workSender ! HashComputationCompleted(workSender, HashedRecord(x.id.toLong, x.lines)) // sending worker the result
      case Failure(error) =>
        handleFailureGracefully(workSender, work, error, noOfRetry)
    }
  }

  override def receive: Receive = idle

  def idle: Receive = {
    case NoWorkToBeDone =>
    case WorkIsAvailable =>
      master ! RequestWork(self)
    case HashToBeComputedFor(data) =>
      log.info("Worker {} got work to be processed with id {}",  self.path.name, data.recId)
      context.become(active)
      doWork(self, HashProcessingJob(data.recId.toString, data.lines))
    case m =>
      log.info("Unhandled message {} at {}. Stashing it for future processing ", m , self.path.name)
  }

  def active: Receive = {
    case NoWorkToBeDone =>
    case RetryHashProcessingJob(retryCount, job) =>
      log.debug("Retrying job: {}. Retry to perform {}", job.id, retryCount)
      doWork(self, job, noOfRetry = retryCount-1)
    case hashComputationCompleted@ HashComputationCompleted(workerRef, hashedData) =>
      log.info("Hash computation completed for id: {}", hashedData.id)
      master ! hashComputationCompleted
      master ! RequestWork(self)
      // changing current context to `idle` as current work have been processed
      context become idle
    case failure@HashProcessingFailedFor(actorRef) =>
      log.info("Hash processing failed for : {}", actorRef.path)
      context.stop(self)
    case m@_ =>
      log.debug("Unhandled message {} at worker: {}", m, self.path.name)
  }

  private def handleFailureGracefully(workerRef: ActorRef, work: HashProcessingJob, error: Throwable, noOfTries: Int): Unit = {
    log.info("{} failed to get response for record with ID: {}. It will be retried {}. Error: {}. ", workerRef.path.name, work.id, noOfTries, error.getMessage)
    if(noOfTries > 0){
      workerRef ! RetryHashProcessingJob(noOfTries,work)
    } else {
      log.error("Hash computation failed for Record Id {} at worker: {}", workerRef.path.name, work.id)
      workerRef ! HashProcessingFailedFor(workerRef)
    }
  }

  override def preStart(): Unit = {
    super.preStart()
    master ! RegisterHashWorker(self)
  }

  override def postStop():Unit ={
    execContext.shutdown()
    super.postStop()
  }
}

object HashComputationWorker{
  def apply() = new HashComputationWorker with DefaultMasterProvider


  case class RegisterHashWorker(actorRef: ActorRef) extends IdentifiableMessage
  case object NoWorkToBeDone extends IdentifiableMessage
  case class HashToBeComputedFor(data: DataRecord) extends IdentifiableMessage
  case class HashProcessingFailedFor(workerRef: ActorRef) extends IdentifiableMessage
  case class HashComputationCompleted(workerRef: ActorRef, hashedData: HashedRecord) extends IdentifiableMessage
}
