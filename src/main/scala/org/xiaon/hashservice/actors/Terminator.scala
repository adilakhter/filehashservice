package org.xiaon.hashservice.actors

import akka.actor.{Terminated, ActorLogging, Actor, ActorRef}
import org.xiaon.hashservice.actors.utils.{DefaultShutdownStrategy, SystemShutdownStrategy}

/**
 * The Terminator actor applies the shutdown pattern to
 * stop the actor system after the completion of the
 * processing.
 *
 * @author Adil Akhter
 */
class Terminator(ref: ActorRef) extends BaseActor { this: SystemShutdownStrategy =>
  context watch ref

  def receive = {
    case Terminated(_) =>
      log.info("{} has terminated, shutting down system.", ref.path)
      shutdown()
  }
}

object Terminator {
  def apply(ref: ActorRef) = new Terminator(ref) with DefaultShutdownStrategy
}
