package org.xiaon.hashservice.actors

import java.util.concurrent.Executors

import akka.actor.SupervisorStrategy.Stop
import akka.actor._
import akka.io.IO
import akka.pattern._
import org.xiaon.hashservice.actors.BaseActor._
import org.xiaon.hashservice.actors.FileReader._
import org.xiaon.hashservice.actors.HashComputationWorker._
import org.xiaon.hashservice.actors.OutputProcessor._
import org.xiaon.hashservice.actors.ServiceMaster._
import org.xiaon.hashservice.model._
import org.xiaon.hashservice.providers._
import org.xiaon.hashservice.config.ConfigHelper._
import spray.can.Http

import scala.collection.immutable.TreeMap
import scala.collection.mutable.{Map, Queue}
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

/**
 * The ServiceMaster is the main actor that applies work pulling pattern
 * to compute hash of the specified file using the rest
 * service named hashing-as-a-service.
 *
 * @author Adil Akhter
 */
class ServiceMaster extends BaseActor {
  this: ReaderProvider with HashProcessorProvider =>

  private[this] val execService = Executors.newCachedThreadPool()
  implicit private[this] val execContext = ExecutionContext.fromExecutorService(execService)

  val inFileEncoding:  String = getConfigOrElse(config.getString(s"$APP_CONFIG_KEY.infile.encoding"))(DEFAULT_FILE_ENCODING)
  val outFileEncoding: String = getConfigOrElse(config.getString(s"$APP_CONFIG_KEY.outfile.encoding"))(DEFAULT_FILE_ENCODING)
  private[this] val recordSize: Int = getConfigOrElse(config.getInt(s"$APP_CONFIG_KEY.recordsize"))(DEFAULT_DATA_RECORD_SIZE)
  private[this] val noOfRecordsToStartWith: Int  = getConfigOrElse(config.getInt(s"$APP_CONFIG_KEY.inital-no-of-records-to-begin"))(INITIAL_RECORDS_TO_READ )
  private[this] val minDataRecordsToRead: Int = getConfigOrElse(config.getInt(s"$APP_CONFIG_KEY.minDataRecordsToRead"))(MIN_DATA_RECORDS_TO_READ_IN_EACH_TIME)

  type ActiveState = Boolean
  // main actors involved in the system 
  private[this] val ioWorkersState   = Map.empty[ActorPath, ActiveState]
  private[this] val hashWorkers      = Map.empty[ActorRef, Option[DataRecord]]
  
  // intermediate storage to keep track about the progress  
  private[this] var activeDataRecords     = TreeMap.empty[Long, DataRecord]
  private[this] var hashedRecordQueue     = Queue.empty[HashedRecord]
  private[this] var activeHashedRecords   = Map.empty[Long, HashedRecord]

  // statistics regarding the computation stat 
  private[this] var totalNumberOfRecords: Long = Long.MinValue // initially not available
  private[this] var noOfRecordsRead: Long = 0
  private[this] var noOfActiveRecords: Long = 0
  private[this] var noOfRecordsProcessed: Long = 0

  // input and out file path 
  private[this] var inputFilePath :   String = _
  private[this] var outputFilePath:   String = _

  override def preStart(): Unit = {
    log.debug("Starting ServiceMaster with following configuration. Input file encoding {}, output file encoding {}", inFileEncoding, outFileEncoding)
    // starting up all the component of the actor system
    setupReader()
    setupWriter()
    setupWorkers()
  }

  /**
   * Sets up the file reader actor.
   */
  private[this] def setupReader(): Unit = {
    context.actorOf(Props(FileReader()), FILE_READER_ID)
  }
  /**
   * Sets up hash processing actor.
   */
  private[this] def setupWriter(): Unit= {
    context.actorOf(Props(OutputProcessor()), OUTPUT_WRITER_ID)
  }

  /**
   * Sets up the hash computation workers using its supervisor.
   */
  private[this] def setupWorkers(): Unit ={
    context.actorOf(Props(HashComputationSupervisor()), HASH_COMPUTATION_LIFECYCLE_SUPERVISOR)
  }

  /**
   * Initializes hash computation.
   * It also notifies relevant actors regarding that
   * fact that hash processing is getting started for the
   * input file name.
   *
   * @param master reference to the master actor
   * @param requestSender actorRef to the sender who initiates the processing
   * @param message an instance of BeginHashComputation the specifies input parameters
   */
  protected[this] def initializeComputation(master: ActorRef, requestSender: ActorRef, message: BeginHashComputation)= {
    initialize(message)
    Future{
      processor ! InitializeWriter(outputFilePath, outFileEncoding)
      reader ! InitializeReader(inputFilePath, inFileEncoding)
      requestSender ! HashComputationStarted
      HashComputationStarted
    } pipeTo master
  }


  override protected[this] def initialize(initMessage: IdentifiableMessage): Unit = {
    val initializeMessage = initMessage.asInstanceOf[BeginHashComputation]

    inputFilePath = initializeMessage.inputFilePath
    outputFilePath = initializeMessage.outputFilePath

    totalNumberOfRecords = Long.MinValue
    noOfRecordsRead = 0
    noOfActiveRecords = 0
    noOfRecordsProcessed = 0

    context.watch(reader)
    context.watch(processor)
  }

  override def receive: Receive = idle

  private[this] def idle: Receive = {
    case message@BeginHashComputation(_, _) =>
      log.info("Initializing {} with message: {}", self.path, message)
      initializeComputation(self, sender(), message)

    case HashComputationStarted =>
      log.info("{} is processing at {}", self.path.name, self.path)
      unstashAll()
      context.become(active)

    case RegisterHashWorker(worker) =>
      log.info("Hash worker is registered with master: {}", worker.path)
      context.watch(worker)
      hashWorkers += (worker -> None)
      broadcastHashComputationWorkAvailable()
    case m@_ =>
      log.debug("Unknown message: {}. Not processing at this point and stashing it.", m)
      stash()
  }

  private[this] def readRecords:Receive = {
    case data@ReceiveDataRecords(records) =>
      for(rec <- records) {
        log.info("Received data to process : {}", rec.recId)
        activeDataRecords += (rec.recId -> rec)
        noOfRecordsRead = noOfRecordsRead + 1
        noOfActiveRecords = noOfActiveRecords + 1
      }
      broadcastHashComputationWorkAvailable()
    case ReaderIsInitializedWith(_, _) =>
      log.info("FileRead actor has been initialized. Actor ref {}", reader.path)
      ioWorkersState += (reader.path -> true)
      requestReadDataRecords(noOfRecordsToStartWith)
    case FileReadCompleted(lastRecID) =>
      log.info("Turning the reader off. Last record Id: {}", lastRecID)
      totalNumberOfRecords = lastRecID
      if(ioWorkersState.contains(reader.path))
        ioWorkersState += (reader.path-> false)

      reader ! GracefullyShutdown

    case ReaderAlreadyInitializedWith =>
  }

  private[this] def hashRecords: Receive = {
    case RegisterHashWorker(worker) =>
      log.info("Hash worker is registered with master: {}", worker.path)
      context.watch(worker)
      hashWorkers += (worker -> None)
      broadcastHashComputationWorkAvailable()
    
    case RequestWork(workRef) if hashWorkers.contains(workRef)=>
      log.info("Worker {} is requesting work", workRef.path.name)
      if(activeDataRecords.isEmpty){
          log.info("No work available for worker: {}", workRef.path.name)
          workRef ! NoWorkToBeDone
          requestReadDataRecords(minDataRecordsToRead)
        }
        else if (hashWorkers(workRef) == None) {
          val dataToProcess = activeDataRecords.head match {case (recId, data) => data}
          hashWorkers += (workRef -> Some(dataToProcess))
          activeDataRecords = activeDataRecords.tail
          log.info("Submitted work to worker: {}", workRef.path.name)
          workRef ! HashToBeComputedFor(dataToProcess)
      }
    case HashComputationCompleted(workerRef, result) =>
      if(hashWorkers.contains(workerRef) && hashWorkers(workerRef) != None){
        hashWorkers += (workerRef -> None)
      }
      hashedRecordQueue += result
      requestProcessingHashedRecords()
  }

  private[this] def processHashedRecords: Receive = {
    case initProcessor@ WriterIsInitializedWith(_, _) =>
      log.info("HashProcessor actor has been initialized. Actor ref {}", processor.path)
      ioWorkersState += (processor.path -> true)
    case RequestWork(ref) if ref.path.name == BaseActor.OUTPUT_WRITER_ID =>
      if(!hashedRecordQueue.isEmpty) {
        // dequeueing the hashed record to be processed by the writer
        val data = hashedRecordQueue.dequeue()
        // storing hashed records to be processed by the processors
        activeHashedRecords += (data.id -> data)
        processor ! ProcessHashedData(data)
        log.info("Processing HashedData with {} using actor {}", data.id, processor.path)
      }
    case processedData@ProcessingHashedDataCompleted(recordIds) =>
      val ids: List[Long] = recordIds.getOrElse(Nil)
      log.info("Master got the processed HashData  #{} IDs from {}", ids.length, processor.path.name)

      ids.foreach {
        recID =>
          activeHashedRecords.remove(recID)
          noOfRecordsProcessed = noOfRecordsProcessed + 1
          noOfActiveRecords = noOfActiveRecords - 1
      }
      stopIfProcessingHashBeenCompleted()
      requestProcessingHashedRecords()

      if(ids != Nil)
        requestReadDataRecords(ids.length)

    case WriterAlreadyInitializedWith(_,_) => // do nothing
  }

  private[this] def active: Receive = readRecords orElse
    hashRecords orElse
    processHashedRecords orElse
    handleTerminated orElse
    processUnhandledMessages

  override protected[this] def shuttingDown: Receive = {
    case GracefullyShutdown =>
      log.info("Shutting down ServiceMaster")
      context.stop(self)
    case m =>
      super.shuttingDown(m)
  }

  private[this] def processUnhandledMessages: Receive = {
    case m =>
      log.error("Unhandled message: `{}` received.", m)
  }

  /**
   * Notifies HashComputationWorker regarding
   * the available works.
   */
  private[this] def broadcastHashComputationWorkAvailable(): Unit = {
    if (!activeDataRecords.isEmpty) {
      hashWorkers.foreach {
        case (worker, m) if (m.isEmpty) => worker ! WorkIsAvailable
        case _ =>
      }
    }
  }

  /**
   * Notify the reader to read more data
   * so that it can be processed further.
   *
   * It verifies that the reader is still
   * alive and then ask it for more work.
   */
  private[this] def requestReadDataRecords(noOfRecords:Int = 1): Unit = {
    if(ioWorkersState(readerPath)) {
      reader ! ReadData(recordSize,noOfRecords)
    }
  }

  /**
   * Requests `OutputProcessor` to process `HashedRecords`
   */
  private[this] def requestProcessingHashedRecords(): Unit = {
    if(!hashedRecordQueue.isEmpty){
      processor !  WorkIsAvailable
    }
  }

  override def supervisorStrategy: SupervisorStrategy = OneForOneStrategy() {
    case exp: Exception =>
      log.debug("Encountered exception : {}. Processing failed and stopping actors.", exp)
      Stop
    case error: Error =>
      log.debug("Encountered error: {}. Processing failed and stopping actors.", error)
      Stop
  }

  private[this] def stopMaster(): Unit = {
    context become (shuttingDown)
    self ! GracefullyShutdown
  }

  private[this] def stopMasterIfWorkersAvailable():Unit = {
    if(hashWorkers.isEmpty){
      log.error("All HashWorkers has been terminated.")
      log.error("System is not in a stable situation. Initiate stopping ServiceMaster.")
      stopMaster()
    }
  }

  private[this] def cleanupHashComputationWorkState(ref: ActorRef): Unit = {
    if(hashWorkers(ref) != None){
      // Since the worker inprogress died, sending message to ourselves
      // to reprocess the work again.
      val dataToProcess:DataRecord = hashWorkers(ref).get
      // Removing the stopped actor from the worker pool
      hashWorkers.remove(ref)
      activeDataRecords += (dataToProcess.recId -> dataToProcess)
      broadcastHashComputationWorkAvailable()
    }
  }

  private[this] def handleTerminated: Receive ={
    case Terminated(ref) if ref.path.name == BaseActor.OUTPUT_WRITER_ID =>
      log.info("Actor {} has been terminated", ref.path.name)
      if(ioWorkersState.contains(ref.path))
        ioWorkersState += (ref.path -> false)
     stopMaster()

    case Terminated(ref) if ref.path.name == BaseActor.FILE_READER_ID =>
      log.debug("Reader `{}` has been terminated", ref.path)
      stopIfProcessingHashBeenCompleted()

    case Terminated(ref) if hashWorkers.contains(ref) =>
      log.info("HashWorker `{}` has been terminated", ref.path)
      cleanupHashComputationWorkState(ref)
      stopMasterIfWorkersAvailable()

    case Terminated(ref) =>
      log.debug("Actor `{}` has been terminated", ref)
  }

  override def postStop(): Unit = {
    log.debug("Stopping actor and its child actor: {}", self.path)
    // gracefully stopping child actors in case
    // it is not stopped earlier.
    // In case of any unhandled error, Master will be stopped
    // and therefore postStop will be stop and clean up
    // resources it holds.
    List(
      List(reader, processor),
      hashWorkers.map(w => w._1))
    .flatten
    .foreach{
      actorRef => gracefulStop(actorRef, 5 seconds)}

    IO(Http)(context.system).ask(Http.CloseAll)(5 second)
    execContext.shutdown()
    super.postStop()
  }

  private[this] def logStats(): Unit = {
    log.info("Current stats: at {} ", self.path.name)
    log.info("-----------------------------------------------")
    log.info("  totalNumberOfRecords     : {}", if(totalNumberOfRecords == Long.MinValue) "N/A" else totalNumberOfRecords)
    log.info("  noOfRecordsRead          : {}", noOfRecordsRead)
    log.info("  noOfRecordsProcessed     : {}", noOfRecordsProcessed)
    log.info("  noOfActiveRecord         : {}", noOfActiveRecords)
    log.info("-----------------------------------------------")


    assert(noOfActiveRecords == noOfRecordsRead - noOfRecordsProcessed, "activeRecord == RecordsRead - RecordsProcessed")
  }

  private[this] def stopIfProcessingHashBeenCompleted(): Unit = {
    logStats()

    if((ioWorkersState(readerPath) == false) &&
      (noOfActiveRecords == 0) &&
      (noOfRecordsProcessed == totalNumberOfRecords) &&
      hashedRecordQueue.isEmpty){

      log.debug("-----------------------------------------------")
      log.debug("Hash computation is successfully completed by {}", self.path.name)
      log.debug("-----------------------------------------------")
      log.debug("Output File: {}", outputFilePath)
      log.debug("-----------------------------------------------")

      // Provide the necessary feedback to the user
      // just in case the Console logger is turned off
      println(s"Hash computation is successfully completed by ${self.path.name}")
      println(s"Output File: ${outputFilePath}")
      println(s"Log File: ./log/application.log") //TODO: Fix the following hard-coded path.

      processor ! HashProcessingCompleted
    }
  }
}



object ServiceMaster{
  def apply() = new ServiceMaster with DefaultReaderProvider with DefaultHashProcessorProvider

  case object WorkIsAvailable extends IdentifiableMessage
  case class RequestWork(actorRef: ActorRef) extends IdentifiableMessage

  case class BeginHashComputation(inputFilePath: String, outputFilePath: String) extends IdentifiableMessage
  case object HashComputationStarted extends IdentifiableMessage
  case object HashComputationTerminated extends IdentifiableMessage


  private final val DEFAULT_FILE_ENCODING = "UTF-8"
  private final val DEFAULT_DATA_RECORD_SIZE = 5
  private final val INITIAL_RECORDS_TO_READ = 1000
  private final val MIN_DATA_RECORDS_TO_READ_IN_EACH_TIME = 5
}

