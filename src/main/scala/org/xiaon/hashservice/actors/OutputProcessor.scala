package org.xiaon.hashservice.actors

import java.io.PrintWriter
import java.util.concurrent.{Executors, ForkJoinPool}

import akka.actor.Actor.Receive
import org.xiaon.hashservice.actors.BaseActor.{GracefullyShutdown, IdentifiableMessage}
import org.xiaon.hashservice.actors.OutputProcessor._
import org.xiaon.hashservice.actors.ServiceMaster.{RequestWork, WorkIsAvailable}
import org.xiaon.hashservice.collection.OrderedHashedDataQueue
import org.xiaon.hashservice.model.HashedRecord
import org.xiaon.hashservice.providers.{DefaultMasterProvider, ServiceMasterProvider}
import org.xiaon.hashservice.io.{FileIOHelper, IOHelper}

import scala.collection.immutable.TreeMap
import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future}
import akka.pattern.pipe

/**
 * OutputProcessor is responsible for processing
 * the computed hash in ascending order wrt the Id of
 * `HashedData` and for writing it to a configured
 * output file.
 *
 * @author Adil Akhter
 */
class OutputProcessor extends BaseActor{ this: ServiceMasterProvider with IOHelper =>
  private[this] val execService = Executors.newCachedThreadPool()
  implicit private[this] val execContext = ExecutionContext.fromExecutorService(execService)

  private[this] var outputFilePath: String = _
  private[this] var outputFileEncoding:String = _
  private[this] var queue: OrderedHashedDataQueue = _
  private[this] var output: PrintWriter = _
  private[this] var nextHashedDataToProcess: Long = _

  private[this] def incrementAndGetNextIdToProcess: Long = {
    nextHashedDataToProcess = nextHashedDataToProcess + 1
    nextHashedDataToProcess
  }

  override def postStop(): Unit ={
    closeStream()
    execService.shutdown()
    super.postStop()
  }

  override protected[this] def initialize(initMessage: IdentifiableMessage): Unit = {
    val initWriter = initMessage.asInstanceOf[InitializeWriter]
    // Setting up file path and encoding
    outputFilePath = initWriter.filePath
    outputFileEncoding = initWriter.encoding
    // setting up intermediate data structure to order hash
    queue = new OrderedHashedDataQueue
    nextHashedDataToProcess = 1;
    // setting output stream
    ensureDirectoriesExists(outputFilePath)
    output = new PrintWriter(outputFilePath, outputFileEncoding)
    //TODO: Exception handling
  }

  /**
   * Stores `data`and then retrieves the relevant HashedRecords
   * that can be processed (i.e., written) in the output stream.
   *
   * @param data  an instance of `HashedRecord` that got stored.
   * @return a collection of HashedRecord to process or None.
   */
  private[this] def getHashedDataToProcess(data: HashedRecord): Option[List[HashedRecord]] = {
    def predicate: Tuple2[Long,HashedRecord] => Boolean = {
      case head@(id, data) => {
        val isEqual= id == nextHashedDataToProcess
        if(isEqual)
          incrementAndGetNextIdToProcess // proceede to process the next `HashedData`
        isEqual
      }
    }
    // Adding `data` to buffer to process However if data.id is less
    // than `nextHashedDataToProcess`,
    // it implies that we have already processed it.
    if(data.id >= nextHashedDataToProcess){
      queue.enqueue(data)

      val hashDataList: List[HashedRecord]  = queue.dequeue(predicate)
      hashDataList match{
        case Nil => None
        case _   => Some(hashDataList)
      }
    }
    else {
      None
    }
  }

  private[this] def idle: Receive = {
    case initMessage@InitializeWriter(_, _) =>
      Future{
        initialize(initMessage)
        log.info("Initialization is completed: output path: {} and file encoding {}", outputFilePath, outputFileEncoding)

        unstashAll()
        context.become(active)

        WriterIsInitializedWith(outputFilePath, outputFileEncoding)

      } pipeTo master
    case m@_ =>
      log.info("Cannot handle the following message now: {} in idle state. So stashing it. ", m)
      stash()
  }

  private[this] def active: Receive = {
    case WorkIsAvailable =>
      log.info("Work is available for {}", self.path.name)
      master ! RequestWork(self)
    case ProcessHashedData(data) =>
      log.info("Processing data with id: {}", data.id)
      val hashedDataToProcessOption: Option[List[HashedRecord]] = getHashedDataToProcess(data)
      val processedDataIDs = writeToFile(hashedDataToProcessOption)
      master ! ProcessingHashedDataCompleted(processedDataIDs)
      master ! RequestWork(self)
    case HashProcessingCompleted =>
      log.info("Writing all HashData is completed. Turning off actor: {}", self)
      closeStream()
      context.become(shuttingDown)
      self !  GracefullyShutdown
    case initMessage@InitializeWriter(_, _) =>
      log.error("Already initialized. OutputWriter received following request: {}", initMessage)
      master ! WriterAlreadyInitializedWith(outputFilePath, outputFileEncoding)
    case m@_ =>
      log.info("Not able to handle the following message now: {}. Stashing it for future processing.", m)
  }

  override def receive: Receive = idle

  /**
   * Writes `dataListOption` to the output stream.
   *
   * @param dataListOption a collection of HashedRecord or None.
   * @return A sequence of Long represents the IDs of the record written to disk.
   */
  private[this] def writeToFile(dataListOption: Option[List[HashedRecord]]): Option[List[Long]] = {
    dataListOption match{
      case None => None
      case Some(dataList) =>
        for(data <- dataList){
          data.hash.foreach{hash =>
            output.println(hash)
          }
        }
        output.flush()
        Some(dataList.map(d => d.id))
    }
  }

  /**
   * Closes the underlying stream.
   */
  private[this] def closeStream(): Unit = {
    if(output != null){
      log.debug("Closing its underlying stream.")
      output.flush()
      output.close()
      output = null
    }
  }
}

/**
 * Defines protocol for OrderedHashProcessor actor.
 */
object OutputProcessor{
  def apply() = new OutputProcessor with DefaultMasterProvider  with FileIOHelper

  case class InitializeWriter(filePath: String, encoding: String) extends  IdentifiableMessage
  case class WriterIsInitializedWith(filePath: String, encoding: String) extends  IdentifiableMessage
  case class WriterAlreadyInitializedWith(filePath: String, encoding: String) extends  IdentifiableMessage

  case class ProcessHashedData(data: HashedRecord) extends  IdentifiableMessage
  case class ProcessingHashedDataCompleted(processedIds: Option[List[Long]]) extends  IdentifiableMessage

  case object HashProcessingCompleted extends IdentifiableMessage
}
