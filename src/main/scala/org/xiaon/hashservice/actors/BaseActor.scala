package org.xiaon.hashservice.actors

import akka.actor.{Stash, ActorLogging, Actor}
import org.xiaon.hashservice.actors.BaseActor.{GracefullyShutdown, IdentifiableMessage}

/**
 * A base actor for the hash generator application. 
 * 
 * @author Adil Akhter
 */
trait BaseActor extends Actor with Stash with ActorLogging {
  val config = context.system.settings.config

  protected final val APP_CONFIG_KEY = "org.xiaon.hashservice"

  protected[this] def initialize(initMessage: IdentifiableMessage): Unit = {
     log.debug("Initializing actor with path: {}", self.path)
   }

  protected[this] def shuttingDown:Receive = {
    case GracefullyShutdown =>
      log.info("Gracefully shutting down actor `{}`", self.path)
      context.stop(self)
    case m @ _ =>
      log.info("Actor {} is shutting down. Hence, Cannot handle the message: {} ", self.path, m)
  }

  override def preStart(): Unit = {
    log.debug("Starting actor: {}", self.path)
  }

  override  def postStop(): Unit ={
    log.debug("Actor {} is stopping... at postStop of BaseActor.", self.path)
    super.postStop()
  }

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    log.error("Actor `{}` is restarting due to {}. Details: {}  and {}" , self.path, reason.getMessage,  reason, message)
    super.preRestart(reason, message)
  }
}

object BaseActor{
  trait IdentifiableMessage
  case object GracefullyShutdown extends  IdentifiableMessage

  final val FILE_READER_ID = "FileReader"
  final val OUTPUT_WRITER_ID = "OutputWriter"
  final val SERVICE_MASTER = "ServiceMaster"
  final val TERMINATOR_ID = "Terminator"
  final val HASH_COMPUTATION_LIFECYCLE_SUPERVISOR = "HashWorkerLifecycleSupervisor"


}
