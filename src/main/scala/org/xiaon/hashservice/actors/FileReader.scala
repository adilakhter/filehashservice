package org.xiaon.hashservice.actors

import java.util.concurrent.Executors

import org.xiaon.hashservice.actors.BaseActor.IdentifiableMessage
import org.xiaon.hashservice.actors.FileReader._
import org.xiaon.hashservice.model.DataRecord
import org.xiaon.hashservice.providers.{DefaultMasterProvider, ServiceMasterProvider}
import akka.pattern._
import scala.concurrent.{ExecutionContext, Future}
import scala.io.Source

/**
 * An actor that reads input file lazily.
 *
 * @author Adil Akhter
 */
class FileReader extends BaseActor {  this: ServiceMasterProvider  =>
  var filePath: String  = _
  var fileEncoding: String = _

  private[this] var source: Source = _
  private[this] var recordNo: Long = _
  var linesToProcess: Iterator[String] = _


  private[this] def idle: Receive = {
    case initMessage@InitializeReader(path, encoding) =>
      log.debug("Initializing reader with file path: {} and encoding: {}",path, encoding)

      initialize(initMessage)
      unstashAll()
      context.become(active)

      master ! ReaderIsInitializedWith(path, encoding)

      log.debug("Initialized reader with file path: {} and encoding: {}", filePath, fileEncoding)
    case m @ _ =>
      log.debug("Cannot handle the following message now: {}. Stashing it. ", m)
      stash()
  }

  private[this] def active: Receive = {
    case initMessage@InitializeReader(path, encoding) =>
      log.error("Already initialized and active with: {} and encoding: {}",filePath, fileEncoding)
      master ! ReaderAlreadyInitializedWith(filePath,fileEncoding)

    //TODO: Use Future to perform the below
    case ReadData(noOfLinesToRead, noOfRecords) =>
      if(linesToProcess.hasNext == false){
        log.info("EOF Reached. Sending self ! FileReadCompleted")
        self ! FileReadCompleted(recordNo)
      } else {
        val lines = linesToProcess.take(noOfLinesToRead * noOfRecords)
        val records = lines.grouped(noOfLinesToRead).map(rec => DataRecord(incrementAndGetRecordSerialNo(), rec.toList))
        master ! ReceiveDataRecords(records.toList)
      }
    case FileReadCompleted(recordID) =>
      log.info("File read completed detected at {} with last record id {}",self.path,recordID)
      master ! FileReadCompleted(recordID)
      source.close()
      context.become(shuttingDown)

    case m @ _ =>
      log.debug("Unhandled message: {}. ", m)
  }

  override def receive: Receive = idle

  override  def postStop(): Unit ={
    closeSource()
    super.postStop()
  }

  override protected[this] def initialize(initMessage: IdentifiableMessage) = {
    val initReaderMessage: InitializeReader =  initMessage.asInstanceOf[InitializeReader]
    filePath = initReaderMessage.filePath
    fileEncoding= initReaderMessage.encoding
    source = Source.fromFile(filePath, fileEncoding)
    linesToProcess = source.getLines()
    recordNo = 0
  }

  private[this] def incrementAndGetRecordSerialNo(): Long = {
    recordNo = recordNo + 1
    recordNo
  }

  private[this] def closeSource() = {
    if (source != null) {
      log.debug("Closing its underlying stream.")
      source.close()
      source = null
    }
  }
}

/**
 *  Defines protocol for FileReader.
 */
object FileReader {
  def apply() = new FileReader with DefaultMasterProvider

  case class InitializeReader(filePath: String, encoding: String) extends IdentifiableMessage
  case class ReaderIsInitializedWith(initializedFilePath: String, initializedEncoding: String) extends IdentifiableMessage
  case class ReaderAlreadyInitializedWith(initializedFilePath: String, initializedEncoding: String) extends IdentifiableMessage
  case class ReadData(noOfLine: Int, noOfRecords: Int = 1) extends IdentifiableMessage
  case class ReceiveDataRecords(dataRecords:List[DataRecord]) extends IdentifiableMessage
  case class FileReadCompleted(lastRecID: Long) extends IdentifiableMessage

}
