package org.xiaon.hashservice.config

import com.typesafe.config.ConfigFactory
import org.slf4j.LoggerFactory
import scala.concurrent.duration._
import scala.util.control.Exception.catching

/**
 * A helper class for loading configuration
 *
 * @author Adil Akhter
 */
object ConfigHelper {
  val LOG = LoggerFactory.getLogger(ConfigHelper.getClass)

  def getConfigOrElse[T](f: => T)(defaultValue:T): T = {
    catching(classOf[Exception]).either(f) match {
      case Right(value) => value
      case Left(error) =>
        LOG.error("Invalid configuration found. See the error log for details.", error)
        LOG.info("Using default value: `{}` for the configuration.", defaultValue)
        defaultValue
    }
  }
}
