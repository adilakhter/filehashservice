package org.xiaon.hashservice

import akka.actor.{Props, ActorSystem}
import akka.event.Logging
import akka.util.Timeout
import org.slf4j.LoggerFactory
import org.xiaon.hashservice.actors.ServiceMaster.BeginHashComputation
import org.xiaon.hashservice.actors.{Terminator, BaseActor, ServiceMaster}
import org.xiaon.hashservice.io.{FileIOHelper, IOHelper}
import scala.concurrent.Await
import scala.concurrent.duration._
import akka.pattern._
import actors.BaseActor._

/**
 * Main class to run the `FileHashService` application. It expects
 * the input file path and output file path to be passed as
 * arguments.
 *
 * It initiates the actor system and and begin the processing of file
 * with the input and output file arguments.
 *
 * @author Adil Akhter
 */
object Main {
  val LOG = LoggerFactory.getLogger(Main.getClass)
  val ioHelper: IOHelper = new FileIOHelper{}

  val INPUT_FILE_PATH_INDEX:Int = 1
  val OUTPUT_FILE_PATH_INDEX:Int = 2

  val system = ActorSystem("FileHashServiceSystem")
  implicit val timeout =  Timeout(5.seconds)

  def main(args: Array[String]) = {
    // Verifying the arguments provided
    verifyArguments(args)

    val inputFilePath: String = args(INPUT_FILE_PATH_INDEX)
    val outputFilePath: String = args(OUTPUT_FILE_PATH_INDEX)

    // Verifying input and output files' details
    ensureInputFileExists(inputFilePath)
    ensureOutputDirectoriesExist(outputFilePath)

    beginComputation(inputFilePath, outputFilePath)

  }

  private def beginComputation(inFilePath: String, outFilePath: String): Unit = {
    // Constructing `service master` actor.
    val serviceMaster = system.actorOf(Props(ServiceMaster()), SERVICE_MASTER)

    // Constructing terminator actor to watch over `serviceMaster`.
    // By employing akka's shutdown pattern, it stops the
    // actor system when `serviceMaster` stops.
    system.actorOf(Props(Terminator(serviceMaster)), BaseActor.TERMINATOR_ID)

    // Starting hash computation of the input file specified using `inputFilePath`
    Await.result(serviceMaster ? BeginHashComputation(inFilePath, outFilePath), 3 seconds)

    // Writing in stdout, to provide end user some feedback regarding the begining of the
    // hash computation.
    println(s"Beginning Hash Computation ${inFilePath}")
  }

  private[this] def verifyArguments(args: Array[String]): Unit = {
    if(args.length < 3){
      LOG.error("Not all the arguments are specified in {}. It should at least specify input and output file path", args)
      System.exit(1)
    }
  }

  private[this] def ensureInputFileExists(inputFilePath: String): Unit = {
    if(!ioHelper.verifyFileExists(Some(inputFilePath))){
      LOG.error("Input file {} does not exists", inputFilePath)
      System.out.println("Specified file:"+inputFilePath+" is not found. " +
        "Please specify the path of a file that exists in the system.")
      System.exit(1)
    }
  }

  def ensureOutputDirectoriesExist(outputFilePath: String): Unit = {
    ioHelper.ensureDirectoriesExists(outputFilePath)
  }
}
