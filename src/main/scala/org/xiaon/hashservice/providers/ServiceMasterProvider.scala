package org.xiaon.hashservice.providers

import akka.actor.{ActorPath, ActorRef}
import org.xiaon.hashservice.actors.BaseActor
import org.xiaon.hashservice.actors.BaseActor._

/**
 *
 * @author Adil Akhter
 */
trait ServiceMasterProvider {
  def master: ActorRef
  def masterPath: ActorPath
}

/**
 * Provides the ActorRef for the service master.
 *
 * @author Adil Akhter
 */
trait DefaultMasterProvider extends ServiceMasterProvider { this: BaseActor =>
  val serviceMasterPathString  = s"/user/$SERVICE_MASTER"

  override def masterPath = ActorPath.fromString(s"akka://${context.system.name}${serviceMasterPathString}")
  override def master: ActorRef = context.actorFor(masterPath)
}
