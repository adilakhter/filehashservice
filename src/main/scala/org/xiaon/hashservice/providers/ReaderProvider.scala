package org.xiaon.hashservice.providers

import akka.actor.{ActorPath, ActorRef}
import org.xiaon.hashservice.actors.BaseActor
import org.xiaon.hashservice.actors.BaseActor._

/**
 *
 * @author Adil Akhter
 */
trait ReaderProvider {
  def reader: ActorRef
  def readerPath: ActorPath
}


trait DefaultReaderProvider extends ReaderProvider{this: BaseActor =>
  val readerRelPath  = s"/user/$SERVICE_MASTER/$FILE_READER_ID"

  override def readerPath = ActorPath.fromString(s"akka://${context.system.name}${readerRelPath}")
  override def reader: ActorRef = context.actorFor(readerPath)
}