package org.xiaon.hashservice.providers

import akka.actor.{ActorPath, ActorRef}
import org.xiaon.hashservice.actors.BaseActor
import org.xiaon.hashservice.actors.BaseActor._

/**
 *
 * @author Adil Akhter
 */
trait HashProcessorProvider {
  def processor: ActorRef
  def processorPath: ActorPath
}

trait DefaultHashProcessorProvider extends HashProcessorProvider{this: BaseActor =>
  val processorRelPath  = s"/user/$SERVICE_MASTER/$OUTPUT_WRITER_ID"

  override def processorPath: ActorPath = ActorPath.fromString(s"akka://${context.system.name}${processorRelPath}")

  override def processor: ActorRef = context.actorFor(processorPath)

}
