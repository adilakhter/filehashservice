package org.xiaon.hashservice.collection

import org.xiaon.hashservice.model.HashedRecord

import scala.collection.immutable.TreeMap
import scala.collection.mutable.ListBuffer

/**
 * A priority queue implementation that keep the `HashedData` be
 * be processed sorted. It also dequeue the data in that order
 * or delay the dequeue process to keep the order.
 *
 * @author Adil Akhter
 */
class OrderedHashedDataQueue{
  var treeMap: TreeMap[Long, HashedRecord] = TreeMap.empty[Long, HashedRecord]

  def enqueue(data: HashedRecord): OrderedHashedDataQueue = {
    treeMap += (data.id -> data)
    this
  }

  def enqueueAll(data: List[HashedRecord]): OrderedHashedDataQueue = {
    for(d <- data){
      enqueue(d)
    }
    this
  }

  def dequeue(f: ((Long, HashedRecord)) => Boolean): List[HashedRecord] = {
    var removedHashedData = new ListBuffer[HashedRecord]

    while(!treeMap.isEmpty  && f(treeMap.head)){
      removedHashedData += (treeMap.head match{case (key, data) =>data})
      treeMap = treeMap.tail
    }
    
    removedHashedData.toList
  }
}
