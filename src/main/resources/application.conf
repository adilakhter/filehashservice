akka {
  #event-handlers = ["akka.event.slf4j.Slf4jEventHandler"]
  loggers = ["akka.event.slf4j.Slf4jLogger"]
  # Log level for the very basic logger activated during AkkaApplication startup
  # Options: OFF, ERROR, WARNING, INFO, DEBUG
  loglevel = "INFO"
  stdout-loglevel =  "INFO"

  log-dead-letters = 10
  log-dead-letters-during-shutdown = on
  # Log the complete configuration at INFO level when the actor system is started.
  # This is useful when you are uncertain of what configuration is used.
  log-config-on-start = off
  actor {
    # guardian supervisor strategy
    guardian-supervisor-strategy = org.xiaon.hashservice.actors.utils.MasterSupervisorStrategyConfigurator

    debug {
      # enable DEBUG logging of actor lifecycle changes
      lifecycle = off
      # enable function of LoggingReceive, which is to log any received message at
      # DEBUG level
      receive = off
      # enable DEBUG logging of subscription changes on the eventStream
      event-stream = off
    }
  }
}

spray.can.host-connector {
  max-connections = 10
  max-retries = 3
  max-redirects = 0
  request-timeout = 60 s
  idle-timeout = 10 s
}

# Application configuration
org.xiaon.hashservice{
  # No of lines in each Record
  recordsize = 5
  # Min data records to read in each request
  minDataRecordsToRead = 5
  # No of records to be read when the computation begins.
  inital-no-of-records-to-begin = 1000

  infile {
    # input File encoding
    encoding = UTF-8
  }
  outfile{
    # output File encoding
    encoding = UTF-8
  }
  # Hash Computation Worker configuration
  hashcomputationworkers{
    # No of Workers that compute hash.
    noOfWorkers = 50
    # Name prefix of workers
    workerNamePrefix = Worker
  }

  hashing-as-a-service{
    # service url
    url = "http://localhost:9000/api/service"
    # No of retries before quiting
    # Since the service fails around 10% of time, trying twice or more
    # should yield the result.
    max-retries = 5
  }
}