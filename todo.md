# 1st Iteration:

[x] add build.sbt 
[x] Add bat file to run the application
[x] Add file helper 
[x] Add file reader actor 
[x] Add file writer actor 
[x] HashComputationWorker and Manager implementations 
[x] Write unit tests
[x] Exception handling 
[x] Documentation 

# 2nd Iteration:

[x] #ACTIVE-RECORDS will be constant 
[+] Work on the TODOs in the code  #INPROGRESS
[+] Add configuration abstraction and validate specified configuration is correct #INPROGRESS
[] More adaptive chunking based on the system's capacity
[] Write huge amount of integration tests for cover the logic in ServiceMaster
[] Add Documentation